import sys, os, math
import tkinter
from tkinter import filedialog
import tkinter as tk

def read_sudoku_from_file(filename):
    with open(filename, "r") as f:
        sudoku_in = []
        for line in f.readlines():
            #converting string line from file to integer list
            int_line = list(map(lambda t: int(t), line.replace('\n', '').split()))
            sudoku_in.append(int_line)
        return sudoku_in

def num_to_cnf(cnf_var, num, invert):
    s = -1 if invert else 1
    #list of binary digits from num, same length as cnf_var
    bin_list = map(lambda t: int(t), bin(num)[2:].zfill(len(cnf_var)))
    new_clause = [*map(lambda t: s*t[0] if t[1] else -s*t[0], zip(cnf_var, bin_list))]
    return new_clause


#add constraint: p and q must be different
def print_different_num_cnf(fp, p, q, n):
    for num in range(1, n+1):
        for p1 in num_to_cnf(p, num, True):
            fp.write(f"{p1} ")
        for q1 in num_to_cnf(q, num, True):
            fp.write(f"{q1} ")
        fp.write("0\n")


def make_cnf_dimacs(sudoku_in, out_file):
    clauses_number = 0
    #dimension of puzzle and minimal number of bits to represent one number
    n = len(sudoku_in[0])
    block = int(math.sqrt(n))
    num_of_bits = len(bin(n)[2:])

    with open(out_file, "w") as f:
        for i in range(1, n*n+1):
            p = [num_of_bits*i - (num_of_bits-1) + offset for offset in range(num_of_bits)]

            #NOTE: CONSTRAINT 1
            #valid numbers in sudoku: from 1 to n
            #restrict all other numbers that can be produced
            forbidden_numbers = [*range(n+1, 1 << num_of_bits)]
            forbidden_numbers.append(0)
            for num in forbidden_numbers:
                for j in num_to_cnf(p, num, True):
                    f.write(f"{j} ")
                clauses_number += 1
                f.write("0\n")

            #NOTE: CONSTRAINT 2
            #add numbers from given input sudoku
            #converting serial number (1-n*n) to index (i, j)
            curr_num = sudoku_in[(i-1)//n][(i-1)%n]
            if curr_num != 0:
                #add all non-zero numbers
                for j in num_to_cnf(p, curr_num, False):
                    f.write(f"{j} 0\n")
                    clauses_number += 1

            #NOTE: CONSTRAINT 3
            #in each row, column and block different numbers from 1 to n
            for j in range(i+1, n*n+1):
                q = [num_of_bits*j - (num_of_bits-1) + offset for offset in range(num_of_bits)]

                if (i-1)//n == (j-1)//n:
                    print_different_num_cnf(f, p, q, n)
                    clauses_number += n
                elif (i-1)%n == (j-1)%n:
                    print_different_num_cnf(f, p, q, n)
                    clauses_number += n
                elif (((i-1)//n)//block, ((i-1)%n)//block) == (((j-1)//n)//block, ((j-1)%n)//block):
                    print_different_num_cnf(f, p, q, n)
                    clauses_number += n

    return n


def ksplit(line, sep, k):
    line = line.split(sep)
    return [line[i:i+k] for i in range(0, len(line), k)]


def cnf_to_num(cnf_clause):
    str_bin = [*map(lambda digit: '1' if int(digit) > 0 else '0' , cnf_clause)]
    return int(''.join(str_bin),2)


def decode_output(sat_output, n):
    num_of_bits = len(bin(n)[2:])

    with open(sat_output, "r") as f:
        solution = f.read()
    if solution[:3] != 'SAT':
        sys.exit(0)

    solution = solution.replace(' 0\n', '').replace('SAT\n', '')
    out_sudoku = "out_sudoku.txt"
    with open(out_sudoku, "w") as g:
        for i, cnf_num in enumerate(ksplit(solution, ' ', num_of_bits)):
            num = cnf_to_num(cnf_num)
            g.write(f"{num} ")
            if (i+1) % n == 0:
                g.write("\n")


def main():
    root = tkinter.Tk()
    root.title("Choose a sudoku file")
    filename = filedialog.askopenfilename()
    sudoku_in = read_sudoku_from_file(filename)
    out_file = "out_sudoku.cnf"
    n = make_cnf_dimacs(sudoku_in, out_file)
    sat_output = "sat_sudoku_solution.txt"
    os.system(f"minisat {out_file} {sat_output}")
    decode_output(sat_output, n)

f=open("out_sudoku.txt")
class ExampleApp(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        t = SimpleTable(self, 16,16)
        t.pack(side="top", fill="x")

class SimpleTable(tk.Frame):
    def __init__(self, parent, rows=16, columns=16):
        # use black background so it "peeks through" to 
        # form grid lines
        tk.Frame.__init__(self, parent, background="black")
        sudoku_out = []
        for line in f.readlines():
            #converting string line from file to integer list
            int_line = list(map(lambda t: int(t), line.replace('\n', '').split()))
            sudoku_out.append(int_line)

        for row in range(rows):
            for column in range(columns):
                number = sudoku_out[row][column]
                label = tk.Label(self, text = number, 
                                 borderwidth=0, width=5)
                label.grid(row=row, column=column, sticky="nsew", padx=1, pady=1)


if __name__ == "__main__":
    main()
    app = ExampleApp()
    app.mainloop()
